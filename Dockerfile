FROM python:alpine
ARG CURATOR_VERSION=5.7.6

RUN pip install -U --quiet elasticsearch-curator==${CURATOR_VERSION}

ENTRYPOINT [ "/usr/local/bin/curator" ]
